import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import LabelBottomNavigation from './LabelBottomNavigation'

import { LinkedinLogo, TwitterLogo, GitlabLogo2 } from './logos'

const styles = theme => ({ // eslint-disable-line
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#424246',
  },
  contact: {
    display: 'flex',
    justifyContent: 'center',
  },
  socialIcon: {
    padding: 16,
    marginTop: 10,
  },
  copyright: {
    display: 'flex',
    textAlign: 'center',
    flexDirection: 'column',
    color: '#fff',
    marginTop: 10,
    marginBottom: 10,
  },
  body1: {
    color: '#fff',
    fontSize: 10,
  },
})

function Footer(props) {
  const { classes } = props
  return (
    <div className={classes.root}>
      <LabelBottomNavigation />
      <div className={classes.contact}>
        <a tabIndex="0" className={classes.socialIcon} role="button" href="https://gitlab.com/fabianstarke" aria-label="Gitlab" title="See my Gitlab profile">
          {GitlabLogo2}
        </a>
        <a tabIndex="0" className={classes.socialIcon} role="button" href="https://www.linkedin.com/in/fabianstarke/" aria-label="Linkedin" title="Linkedin Profile">
          {LinkedinLogo}
        </a>
        <a tabIndex="0" className={classes.socialIcon} role="button" href="https://twitter.com/Fabstarke" aria-label="Twitter" title="Follow me on Twitter">
          {TwitterLogo}
        </a>
      </div>
      <div className={classes.copyright}>
        <Typography className={classes.body1} aligncenter="true" variant="body1">@2018 All rights reserved</Typography>
        <Typography className={classes.body1} variant="body1">Developped by Fabian Starke with React</Typography>
      </div>
    </div>
  )
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line
}

export default withStyles(styles)(Footer)
