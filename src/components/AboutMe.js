// eslint-disable-prefer-stateless-function
import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Resume from '../assets/Resume.pdf'

const images = [
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage1_S1H8aT5hE.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage2_Sk0Up65hV.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage3_Bkbvap934.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage4_HJ9HRpc3N.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage5_HylJwaTc2E.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage6_rkx9r0693V.jpg',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage7_r1qSAp9hV.JPG',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AboutImage8_Hkbkw66qhV.JPG',
  },
]

const styles = theme => ({
  mainContainer: {
    backgroundColor: '#eee',
    textAlign: 'center',
  },
  container: {
    marginTop: 2,
    textAlign: 'center',
    paddingBottom: 50,
  },
  imageContainer: {
    backgroundImage: `url(${'https://ik.imagekit.io/ptebz7oxk/backgroundImage2_HyMSTp524.jpg'})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
    height: 500,
    backgroundColor: 'transparent',
    paddingBottom: '10px',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    // [theme.breakpoints.down('xs')]: {
    //   backgroundAttachment: 'scroll',
    //   height: 500,
    // },
  },
  h1: {
    fontSize: '1.5rem',
    fontWeight: 400,
    textAlign: 'center',
    marginBottom: 40,
    marginTop: 20,
    color: '#607D8B',
    [theme.breakpoints.up('md')]: {
      fontSize: 48,
      fontWeight: 400,
    },
  },
  body1: {
    marginBottom: 20,
    [theme.breakpoints.down('xs')]: {
      marginLeft: 20,
      marginRight: 20,
    },
  },
  contained: {
    marginTop: 20,
    marginBottom: 20,
  },
  resumeButton: {
    textDecoration: 'none',
    color: '#fff',
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    margin: 2,
  },
  gridList: {
    maxWidth: 500,
  },
})

function AboutMe(props) {
  const { classes } = props
  return (
    <div className={classes.mainContainer}>
      <div className={classes.imageContainer} />
      <Grid container direction="row">
        <Grid item xs />
        <Grid item sm={6} xl={4}>
          <Grid className={classes.container} container direction="column">
            <Grid item>
              <Typography className={classes.h1} variant="h1">
                MY STORY
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                align="justify"
                variant="body1"
                className={classes.body1}
              >
                I have studied hotel management and came lately to web
                development, however I have always been interested by the
                digital world and everything related to it.
              </Typography>
              <Typography
                align="justify"
                variant="body1"
                className={classes.body1}
              >
                Some years ago I was bored by my job and was starting to think about new stuff I could do. Beside of my job, I started studying digital marketing and
                working for a young startup where I was in relation with web
                developers. It was not easy to pursuie both works but it offered me great new opportunities and led me
                slowly to decide on choosing web development as a new career
                path.
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                align="justify"
                variant="body1"
                className={classes.body1}
              >
                I started following courses on CodeAcademy, FreecodeCamp, Udemy and some others and finished some certificates with hundreds of hours work commitment. I was reading news realted to technologies on a daily basis, tried to resolve coding challenges and I contributed to open source projects. Finally I decided I was ready for interviewing and getting a position as a frontend developer. I already worked for a web agency as a freelancer and could integrate several projects and I joined lately a young startup.
              </Typography>
            </Grid>
          </Grid>
          <div className={classes.root}>
            <GridList className={classes.gridList} cellHeight={140} cols={2}>
              {images.map(data => (
                <GridListTile key={data.img}>
                  <img
                    className={classes.img}
                    src={data.img}
                    alt={data.title}
                  />
                </GridListTile>
              ))}
            </GridList>
          </div>
          <Button
            className={classes.contained}
            size="medium"
            variant="contained"
            color="primary"
          >
            <Link
              className={classes.resumeButton}
              to={Resume}
              onClick={(event) => {
                event.preventDefault()
                window.open(Resume)
              }}
            >
              View Resume
            </Link>
          </Button>
        </Grid>
        <Grid item xs />
      </Grid>
    </div>
  )
}
AboutMe.propTypes = {
  classes: PropTypes.any.isRequired, // eslint-disable-line
}

export default withStyles(styles)(AboutMe)
