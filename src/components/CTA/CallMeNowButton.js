import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import TextField from '@material-ui/core/TextField'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import InputAdornment from '@material-ui/core/InputAdornment'
import firebase from '../../firebase'
import ReactGA from 'react-ga'

const styles = theme => ({ // eslint-disable-line
  closeIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  contained: {
    marginRight: 5,
    [theme.breakpoints.up('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 20,
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: 24,
    },
  },
  snackbar: {
    display: 'none',
    color: '#fff',
    marginBottom: 20,
    padding: 10,
    backgroundColor: '#2196f3',
  },
})

const projectStage = [
  {
    value: 'Strategy Definition',
    label: 'Strategy Definition',
  },
  {
    value: 'Design/Wireframe',
    label: 'Design/Wireframe',
  },
  {
    value: 'Feasibility Study',
    label: 'Feasibility Study',
  },
  {
    value: 'Prototyping',
    label: 'Prototyping',
  },

]
const budget = [
  {
    value: '0-1',
    label: 'less than 1000€',
  },
  {
    value: '1-5',
    label: 'Between 1000€ and 5000€',
  },
  {
    value: '6-10',
    label: 'Between 6000 and 10000€',
  },
  {
    value: '10-more',
    label: 'More than 10 000€',
  },
]
const mainObjective = [
  {
    value: 'Drive traffic / generate leads',
    label: 'Drive traffic / generate leads',
  },
  {
    value: 'Help close deals',
    label: 'Help close deals',
  },
  {
    value: 'Improve customers satisfaction',
    label: 'Improve customers satisfaction',
  },
  {
    value: 'Increase profitability',
    label: 'Increase profitability',
  },
  {
    value: 'Website management',
    label: 'Website Management',
  },
  {
    value: 'Integrate with other systems',
    label: 'Integrate with other systems',
  },
]
const applicationType = [
  {
    value: 'Mobile App',
    label: 'Mobile App',
  },
  {
    value: 'Web App',
    label: 'Web App',
  },
  {
    value: 'Web & Mobile App',
    label: 'Web & Mobile App',
  },
]

class CallMeNowButton extends Component {
  static propTypes = {
    classes: PropTypes.any.isRequired, // eslint-disable-line
    fullScreen: PropTypes.bool.isRequired,
  }

  state = {
    open: false,
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    date: '',
    time: '',
    projectStages: '',
    projectDescription: '',
    budgets: '',
    applicationTypes: '',
    mainObjectives: '',
  };

  handleClickOpen = () => {
    this.setState({ open: true })
    ReactGA.event({
      category: 'User',
      action: 'Opened quote form'
    })
  };

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    })
  }

  submit = (event) => {
    const {
      firstName,
      lastName,
      email,
      phone,
      date,
      time,
      projectStages,
      projectDescription,
      budgets,
      mainObjectives,
      applicationTypes,
    } = this.state
    // eslint-disable-next-line max-len
    const validFirstName = firstName.length > 2
    const validLastName = lastName.length > 2
    const validEmail = email.length > 5 && email.includes('@') && email.includes('.')
    if (validLastName && validFirstName && validEmail) {
      this.setState({
        lastName,
        firstName,
        email,
        phone,
        date,
        time,
        projectStages,
        projectDescription,
        budgets,
        mainObjectives,
        applicationTypes,
      })
      document.getElementById('snackbar').style.display = 'block'
      document.getElementById('snackbar').style.backgroundColor = 'green'
      document.getElementById('snackbar').textContent = 'The form was submitted and I will get back to you soon'
      setTimeout(() => {
        this.setState({ open: false })
      }, 2000)
      // eslint-disable-next-line max-len
      this.saveMessage(firstName, lastName, phone, email, date, time, projectStages, projectDescription, budgets, mainObjectives, applicationTypes)
      ReactGA.event({
      category: 'User',
      action: 'Quote Sent'
    })
    } else if (!validLastName) {
      event.preventDefault()
      this.notValid('Please type a correct name')
    } else if (!validFirstName) {
      event.preventDefault()
      this.notValid('Please type a correct first name')
    } else if (!validEmail) {
      event.preventDefault()
      this.notValid('Please enter in a correct e-mail adress')
    } else {
      event.preventDefault()
      this.notValid('Please fill in all required fields')
    }
  }

  notValid = (err) => {
    document.getElementById('snackbar').style.display = 'block'
    document.getElementById('snackbar').style.backgroundColor = 'red'
    document.getElementById('snackbar').textContent = `${err}`
    console.log(err)
    setTimeout(() => {
      document.getElementById('snackbar').style.display = 'none'
    }, 2000)
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  saveMessage = (
    firstName,
    lastName,
    phone,
    email,
    date,
    time,
    projectStages,
    projectDescription,
    budgets,
    mainObjectives,
    applicationTypes,
  ) => {
    const messageRef = firebase.database().ref('newContact')
    const newMessageRef = messageRef.push()
    newMessageRef.set({
      firstName,
      lastName,
      phone,
      email,
      date,
      time,
      projectDescription,
      projectStages,
      budgets,
      mainObjectives,
      applicationTypes,
    })
  }

  render() {
    const { classes, fullScreen } = this.props
    const {
      open,
      projectStages,
      applicationTypes,
      budgets,
      mainObjectives,
    } = this.state
    return (
      <div>
        <Button
          className={classes.contained}
          variant="contained"
          size="small"
          color="primary"
          onClick={this.handleClickOpen}
        >
          ASK FOR A QUOTE
        </Button>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={this.handleClose}
          aria-labelledby="dialog"
        >
          <DialogTitle>CONTACT ME</DialogTitle>
          <div id="snackbar" className={classes.snackbar} />
          <DialogContent>
            <DialogActions>
              <IconButton
                className={classes.closeIcon}
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </DialogActions>
          </DialogContent>
          <DialogContent>
            <DialogContentText>
              In order to get back to you please provide some information by completing the following form, I can getin touch with you on a specif date and time if you want to: {/* eslint-disable-line */}  
            </DialogContentText>
            <form>
              <TextField
                autoFocus
                name="date"
                margin="dense"
                id="date"
                label="Date"
                data-parse="date"
                type="date"
                fullWidth
                onChange={e => this.setState({ date: e.target.value })}
              />
              <TextField
                name="time"
                margin="dense"
                id="time"
                label="Best time to call me back"
                fullWidth
                onChange={e => this.setState({ time: e.target.value })}
              />
              <TextField
                required
                name="first name"
                margin="dense"
                id="first-name"
                label="First Name"
                fullWidth
                onChange={e => this.setState({ firstName: e.target.value })}
              />
              <TextField
                required
                margin="dense"
                id="last-name"
                name="last name"
                label="Last Name"
                type="last-name"
                fullWidth
                onChange={e => this.setState({ lastName: e.target.value })}
              />
              <TextField
                margin="dense"
                name="phone"
                id="phone"
                label="Phone Number"
                type="number"
                fullWidth
                onChange={e => this.setState({ phone: e.target.value })}
              />
              <TextField
                required
                margin="dense"
                id="name"
                name="name"
                label="Email Address"
                type="email"
                fullWidth
                onChange={e => this.setState({ email: e.target.value })}
              />
              <TextField
                margin="dense"
                name="project description"
                id="project-description"
                label="Project Description"
                fullWidth
                onChange={e => this.setState({ projectDescription: e.target.value })
                }
              />
              <TextField
                id="project-stage"
                name="project stage"
                className={classes.textField}
                select
                value={projectStages}
                onChange={this.handleChange('projectStages')}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
                fullWidth
                helperText="Project Stage"
              >
                {projectStage.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </TextField>
              <TextField
                id="budget"
                name="budget"
                className={classes.textField}
                select
                value={budgets}
                onChange={this.handleChange('budgets')}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">€</InputAdornment>
                  ),
                }}
                margin="normal"
                fullWidth
                helperText="Budget"
              >
                {budget.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </TextField>
              <TextField
                id="main-objectives"
                name="main objectives"
                className={classes.textField}
                select
                value={mainObjectives}
                onChange={this.handleChange('mainObjectives')}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
                fullWidth
                helperText="Main Objectives"
              >
                {mainObjective.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </TextField>
              <TextField
                id="application-type"
                className={classes.textField}
                name="application type"
                select
                value={applicationTypes}
                onChange={this.handleChange('applicationTypes')}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                margin="normal"
                fullWidth
                helperText="Application Type"
              >
                {applicationType.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </TextField>
            </form>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              size="medium"
              fullWidth
              color="primary"
              onClick={this.submit}
            >
              SEND
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withMobileDialog()(withStyles(styles)(CallMeNowButton))
