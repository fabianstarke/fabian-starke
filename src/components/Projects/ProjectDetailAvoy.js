import React from 'react'
import ProjectDetail from './ProjectDetail'
import { AVOY } from './ProjectDetailsData'


export default function ProjectDetailAvoy() {
  return (
    <div>
      {AVOY.map(data => (
        <ProjectDetail
          key={data.projectName}
          logo={data.logo}
          id={data.id}
          projectName={data.projectName}
          headline={data.headline}
          type={data.type}
          mainImg={data.mainImg}
          p1={data.p1}
          p2={data.p2}
          exampleImage={data.exampleImage}
          exampleImage2={data.exampleImage2}
          tech1={data.tech1}
          techLogo1={data.techLogo1}
          tech2={data.tech2}
          techLogo2={data.techLogo2}
          tech3={data.tech3}
          techLogo3={data.techLogo3}
          tech4={data.tech4}
          techLogo4={data.techLogo4}
          repo={data.repo}
          link={data.link}
          nextProject={data.nextProject}
          previousProject={data.previousProject}
        />
      ))}
    </div>
  )
}
