import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'

const styles = theme => ({ // eslint-disable-line
  container: {
    marginTop: 40,
    textAlign: 'center',
  },
  headContainer: {
    justifyContent: 'space-evenly',
    textAlign: 'center',
    marginTop: 40,
    marginBottom: 40,
    alignItems: 'center',
  },
  logo: {
    marginBottom: 20,
  },
  // projectImage: {
  //   width: '100%',
  //   [theme.breakpoints.up('md')]: {
  //     maxHeight: 400,
  //     objectFit: 'cover',
  //   },
  //   [theme.breakpoints.up('xl')]: {
  //     maxHeight: 600,
  //     objectFit: 'cover',
  //   },
  //   height: 'auto',
  //   filter: 'grayscale(100%)',
  //   marginTop: 20,
  //   marginBottom: 20,
  // },
  exampleImage: {
    width: '100%',
    height: 'auto',
    marginBottom: 10,
    marginTop: 10,
    [theme.breakpoints.down('sm')]: {
      width: '80%',
    },
  },
  exampleContainer: {
    marginBottom: 20,
    justifyContent: 'center',
  },
  h1: {
    fontSize: '1.5rem',
    fontWeight: 'bold',
    marginBottom: 20,
    [theme.breakpoints.up('xs')]: {
      marginLeft: 20,
      marginRight: 20,
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: 34,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 68,
    },
  },
  h2: {
    fontSize: '1rem',
    fontWeight: 'bold',
    marginBottom: 10,
    [theme.breakpoints.up('xs')]: {
      marginLeft: 20,
      marginRight: 20,
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: 28,
    },
  },
  body1: {
    textAlign: 'justify',
    [theme.breakpoints.down('sm')]: {
      marginTop: 10,
    },
    [theme.breakpoints.up('xs')]: {
      fontSize: 14,
      marginLeft: 20,
      marginRight: 20,
    },
    [theme.breakpoints.up('sm')]: {
      marginLeft: 40,
      marginRight: 40,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 24,
      marginLeft: 20,
      marginRight: 20,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 34,
      marginLeft: 20,
      marginRight: 20,
    },
  },
  subtitle1: {
    marginBottom: 20,
    [theme.breakpoints.up('sm')]: {
      fontSize: 24,
    },
  },
  techList: {
    justifyContent: 'center',
  },
  iconContainer: {
    justifyContent: 'space-evenly',
    [theme.breakpoints.up('1930px')]: {
      justifyContent: 'center',
    },
  },
  techIcon: {
    [theme.breakpoints.down('sm')]: {
      padding: 5,
    },
  },
  icon: {
    marginBottom: 5,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: 10,
      paddingRight: 10,
    },
  },
  iconText: {
    fontSize: 10,
    [theme.breakpoints.up('sm')]: {
      fontSize: 12,
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: 20,
    },
  },
  root: {
    fontSize: 12,
  },
  contained: {
    backgroundColor: '#424246',
    color: '#fff',
  },
  buttonsWrapper: {
    display: 'flex',
    justifyContent: 'center',
  },
  fab: {
    margin: '0 5px',
  },
})

function ProjectDetail(props) {
  const {
    classes,
    logo,
    id,
    projectName,
    headline,
    type,
    p1,
    p2,
    exampleImage,
    exampleImage2,
    exampleImage3,
    tech1,
    techLogo1,
    tech2,
    techLogo2,
    tech3,
    techLogo3,
    tech4,
    techLogo4,
    repo,
    link,
    nextProject,
    previousProject,
  } = props
  return (
    <Grid
      container
      direction="row"
      style={{ marginTop: 40, marginBottom: 40 }}
    >
      <Grid item xs />
      <Grid item md={8}>
        <Grid container className={classes.container} direction="column">
          {/* Titles and description */}
          {
            logo ? 
               <Grid item>
            <img
              className={classes.logo}
              src={logo}
              alt={projectName}
              key={id}
              height={window.innerWidth > 1200 ? 120 : 60}
            />
          </Grid> : null
          }
         
          <Grid item>
            <Grid container className={classes.headContainer} direction="row">
              <Grid item md={4}>
                <Typography className={classes.h1} variant="h1">
                  {projectName}
                </Typography>
                <Typography className={classes.h2} variant="h2">
                  {headline}
                </Typography>
                <Typography className={classes.subtitle1} variant="subtitle1">
                  {type}
                </Typography>
                {/* List of techs used */}
                <Grid item className={classes.techList}>
                  <Grid
                    container
                    direction="row"
                    className={classes.iconContainer}
                  >
                    <Grid item md={2} className={classes.techIcon}>
                      <Grid container direction="column">
                        <Grid item className={classes.icon}>
                          {techLogo1}
                        </Grid>
                        <Grid item className={classes.iconText}>
                          {tech1}
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item md={2} className={classes.techIcon}>
                      <Grid container direction="column">
                        <Grid item className={classes.icon}>
                          {techLogo2}
                        </Grid>
                        <Grid item className={classes.iconText}>
                          {tech2}
                        </Grid>
                      </Grid>
                    </Grid>
                    {techLogo3 && (
                      <Grid item md={2} className={classes.techIcon}>
                        <Grid container direction="column">
                          <Grid item className={classes.icon}>
                            {techLogo3}
                          </Grid>
                          <Grid item className={classes.iconText}>
                            {tech3}
                          </Grid>
                        </Grid>
                      </Grid>
                    )}
                    {techLogo4 && (
                      <Grid item md={2} className={classes.techIcon}>
                        <Grid container direction="column">
                          <Grid item className={classes.icon}>
                            {techLogo4}
                          </Grid>
                          <Grid item className={classes.iconText}>
                            {tech4}
                          </Grid>
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item md={6}>
                <Typography
                  className={classes.body1}
                  paragraph
                  variant="body1"
                >
                  {p1}
                </Typography>
                <Typography
                  className={classes.body1}
                  paragraph
                  variant="body1"
                >
                  {p2}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          {/* ScreenShots */}
          <Grid item>
            <Grid className={classes.exampleContainer} container>
              <Grid item>
                <img
                  className={classes.exampleImage}
                  src={exampleImage}
                  alt={projectName}
                />
              </Grid>
              {exampleImage2 && (
                <Grid item>
                  <img
                    className={classes.exampleImage}
                    src={exampleImage2}
                    alt={projectName}
                  />
                </Grid>
              )}
              {exampleImage3 && (
                <Grid item>
                  <img
                    className={classes.exampleImage}
                    src={exampleImage3}
                    alt={projectName}
                  />
                </Grid>
              )}
            </Grid>
          </Grid>
          {/* BottomPage Buttons */}
          <Grid item>
            <Grid
              container
              direction="row"
              className={classes.buttonsWrapper}
            >
              {/* Navigate to previous project */}
              <Grid item className={classes.fab}>
                <Fab
                  size="small"
                  color="primary"
                  aria-label="Next"
                  component={Link}
                  to={previousProject}
                >
                  <Icon>navigate_before</Icon>
                </Fab>
              </Grid>
              {/* View Projects repository if there is one */}
              <Grid item>
                {repo && (
                  <Button
                    href={repo}
                    size={window.innerWidth > 1000 ? 'large' : 'medium'}
                    variant="contained"
                    color="primary"
                  >
                    Repository
                  </Button>
                )}
              </Grid>
              {/* View Projects site if there is one */}
              <Grid item>
                {link && (
                  <Button
                    style={{ marginLeft: 5 }}
                    href={link}
                    size={window.innerWidth > 1000 ? 'large' : 'medium'}
                    variant="contained"
                    color="secondary"
                  >
                    Website
                  </Button>
                )}
              </Grid>
              {/* Navigate to next project */}
              <Grid item className={classes.fab}>
                <Fab
                  size="small"
                  color="primary"
                  aria-label="Next"
                  component={Link}
                  to={nextProject}
                >
                  <Icon>navigate_next</Icon>
                </Fab>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs />
    </Grid>
  )
}
ProjectDetail.propTypes = {
  classes: PropTypes.any.isRequired, // eslint-disable-line
  logo: PropTypes.any, // eslint-disable-line
  id: PropTypes.number.isRequired,
  projectName: PropTypes.string.isRequired,
  headline: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  p1: PropTypes.string, // eslint-disable-line
  p2: PropTypes.string, // eslint-disable-line
  exampleImage: PropTypes.any.isRequired, // eslint-disable-line
  exampleImage2: PropTypes.any, // eslint-disable-line
  exampleImage3: PropTypes.any, // eslint-disable-line
  tech1: PropTypes.string.isRequired,
  techLogo1: PropTypes.object.isRequired, // eslint-disable-line
  tech2: PropTypes.string.isRequired,
  techLogo2: PropTypes.object.isRequired, // eslint-disable-line
  tech3: PropTypes.string, // eslint-disable-line
  techLogo3: PropTypes.object, // eslint-disable-line
  tech4: PropTypes.string, // eslint-disable-line
  techLogo4: PropTypes.object, // eslint-disable-line
  repo: PropTypes.string, // eslint-disable-line
  link: PropTypes.string, // eslint-disable-line
  nextProject: PropTypes.string.isRequired, // eslint-disable-line
  previousProject: PropTypes.string.isRequired // eslint-disable-line
}
export default withStyles(styles)(ProjectDetail)
