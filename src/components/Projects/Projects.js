import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Grid from '@material-ui/core/Grid'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import LazyLoad from 'react-lazyload'
import CircularProgress from '@material-ui/core/CircularProgress'
import uuid from 'uuid/v4'

const projects = [
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/DevDashboardProject_RxYVw7cLn.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/mobileDevDashBoard_p2lz12zL8.jpg',
    projectName: 'DEV DASHBOARD',
    h5: 'MERN Application',
    type: 'Web & Mobile application',
    id: uuid(),
    pathname: '/DevDashboard',
    picAuthor: ' Domenico Loia ',
    picSource: 'https://unsplash.com/photos/hGV2TfOh0ns?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'This is my second project developed with the chingu open source community. This is my first fullstack project using the MERN stack. The main target for this application are developers. On this dashboard the user can add a todo list, personal notes and write wikis.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/goeland_cxMnFALRw.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/goelandMobile_uwP4vGUwK.jpg',
    projectName: 'Restaurant Le Goëland.',
    h5: 'Website',
    type: 'Progressive Web App (PWA)',
    id: uuid(),
    pathname: '/Goeland',
    picAuthor: ' André Gremillet ',
    picSource: 'https://www.youtube.com/user/JaleoStudio',
    paragraph:
      'I used Gatsby a static site generator to build this restaurant website. The owner wanted to be able to show his video and give some major information to his customers. An important point was also to setup a comments page, I used disqus for this purpose.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/AvoyProject_HkNBACj2V.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileAvoy_rJgTIGFp24.jpg',
    projectName: 'AVOY INC.',
    h5: 'Travel app',
    type: 'Web & Mobile application',
    id: uuid(),
    pathname: '/Avoy',
    picAuthor: ' Ross Parmly ',
    picSource: 'https://unsplash.com/photos/rf6ywHVkrlY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'Avoy is a startup that is focused on using data and technology to improve the way we travel. With user data and preferences we can help users find destinations of their dreams and share those experiences with friends, family and fellow travelers',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/ChinguProject_Bk22qSo3V.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileChingu_HyxhUztahV.jpg',
    projectName: 'THE CHINGU PROJECT',
    h5: 'Dashboard for Developers',
    type: 'Chrome Extension',
    id: uuid(),
    pathname: '/Chingu',
    picAuthor: ' Helena Lopes ',
    picSource: 'https://unsplash.com/photos/PGnqT0rXWLs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'The Chingu project is a free colloaboration community for developers and designers that launches collaborative learning cohorts and facilitate remote team project experiences and level-up opportunities for self-directed learners.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/WeddingProject_H101dRs2N.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileWedding_rJZjMtahE.jpg',
    projectName: 'WEDDING PAGE',
    h5: 'Landing Page',
    type: 'Website',
    id: uuid(),
    pathname: '/WeddingPage',
    picAuthor: ' Beatriz Pérez Moya ',
    picSource: 'https://unsplash.com/photos/M2T1j-6Fn8w?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'This project is a very personal one as I designed and developed this website for my wedding. The idea was to present all informations related to this special day to our invitees. It will also be a great place for everybody to share their pictures of this memorable day.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/rsz_backgroundimage__2__Hki1AgxaE.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileBackground_ryonS1nhN.jpg',
    projectName: 'MY PORTFOLIO PAGE',
    h5: 'Mobile First personal website',
    type: 'Website',
    id: uuid(),
    pathname: '/MyPortfolio',
    picAuthor: ' Fabian Starke ',
    picSource: 'https://unsplash.com/photos/Hm_qBQ95Vtg',
    paragraph:
      'I decided to create my own portfolio page in order to gather all my work together and tell my story in the developpement industry. This website has been built from scratch to showcase the technologies I am proficient with, give some details about the latest projects I have been working.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/CdaProject_S1NqRCo3V.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileAvoy_rJgTIGFp24.jpg',
    projectName: 'CARRE DARTISTES',
    h5: 'Artist’s shop Back office ',
    type: 'Web application',
    id: uuid(),
    pathname: '/CarreDartistes',
    picAuthor: ' Eddy Klaus ',
    picSource: 'https://unsplash.com/photos/BHNxfaeNCTI?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'My mission was to integrate the frontend developers teams to build a back office for the management of artists shops to manage artists, stocks, orders paintings and frames. The application also integrates an accounting tool and a chat.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/ibizaProject_Byh7p0snE.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileIbiza_SJl0IMt634.jpg',
    projectName: 'IBIZA',
    h5: 'Accounting Platform ',
    type: 'Web application',
    id: uuid(),
    picAuthor: ' Dmitry Moraine ',
    picSource: 'https://unsplash.com/photos/eBWzFKahEaU?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    pathname: '/Ibiza',
    paragraph:
      'My mission was working on the frontend part of the project. We had to deliver a proof of concept to the client for this project. The goal was to show the feasibility of setting up a new software architechture on a concrete project',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/axtradeProject_S15AZBin4.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileAxtrade_Skb2UMFT2N.jpg',
    projectName: 'AXTRADE',
    h5: 'Brokers platform  ',
    type: 'Web application',
    id: uuid(),
    picAuthor: ' Christine Roy ',
    picSource: ' https://unsplash.com/photos/ir5MHI6rPg0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    pathname: '/Axtrade',
    paragraph:
      'Axtrade is an all-in-one solution to ease the daily work of brokers. It is a Management Platform for Brokers who can manage their contacts and do asset management. The main feature is also an integrated chat in order for brokers to propose new products and negotiate rates.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/FlashProject_HydW2Rs3V.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileFlash_Hk0UMFT2N.jpg',
    projectName: 'FLASH BRUXELLOIS',
    h5: 'Book landing page',
    type: 'Website',
    id: uuid(),
    pathname: '/FlashBruxellois',
    picAuthor: ' Marius badstuber ',
    picSource: 'https://unsplash.com/photos/wFHqJCuGB1g?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'I realized this landing page for 2 reasons: Building quickly a simple website but also helping my father who is a writer to have a working website to be able to sell his book. It was a great experience not only for development but also in design and book edition.',
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/StacesProject_HyDw3Ao3N.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileStaces_BkCUGKT2E.jpg',
    projectName: 'STACES',
    h5: 'Business Dashboard',
    type: 'Prototype',
    id: uuid(),
    pathname: '/Staces',
    picAuthor: ' Stephen Dawson ',
    picSource: 'https://unsplash.com/photos/qwtCeJ5cLYs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      "This is a project that only got until the UI stage due to financial restrictions. The company couldn't go further with the development of the app. This was a great first timer for me to create entirely the design on my own.",
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/ExpatProject_By7TT0snE.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileExpat_BypLMtT3N.jpg',
    projectName: 'Réflexions dun expatrié',
    h5: 'Writers Blog',
    type: 'Website',
    id: uuid(),
    pathname: '/ReflexionsDexpat',
    picAuthor: ' Green Chameleon ',
    picSource: 'https://unsplash.com/photos/s9CC2SKySJM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      "Refléxions d'un expatrié is a blog displaying articles from different sources from a same Author, Wolf Starke. Flash Bruxellois and also l'européen. I have used wordpress with several plugins, maintain them and refresh monthly the content.",
  },
  {
    img: 'https://ik.imagekit.io/ptebz7oxk/ZiloProject_H1yA30jnV.jpg',
    imgMobile: 'https://ik.imagekit.io/ptebz7oxk/Small_devices/mobileZilo_BJ7DmY62N.jpg',
    projectName: 'ZILO INTERNATIONAL',
    h5: 'Corporate Website',
    type: 'Website',
    id: uuid(),
    pathname: '/Zilo',
    picAuthor: ' Riccardo Annandale ',
    picSource: 'https://unsplash.com/photos/7e2pe9wjL9M?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
    paragraph:
      'My first project ever in the web industry for a company. I got hired as an intern and was in charge of the digital marketing aspects of the company and therefore needed to showcase the work of this startup. This project lead me to my choose my new career path, web development',
  },
]

const styles = theme  => ({ // eslint-disable-line
  projectPage: {
    paddingBottom: 10,
    backgroundColor: '#eee',
    paddingTop: 50,
    [theme.breakpoints.up('2000')]: {
      paddingTop: 100,
    },
  },
  card: {
    margin: 10,
    maxWidth: 500,
    borderRadius: 15,
    [theme.breakpoints.up('md')]: {
      width: 400,
    },
    [theme.breakpoints.up('lg')]: {
      width: 450,
    },
  },
  container: {
    position: 'relative',
  },
  media: {
    height: 200,
    objectFit: 'cover',
    [theme.breakpoints.up('sm')]: {
      height: 300,
    },
    [theme.breakpoints.up('md')]: {
      height: 350,
    },
  },
  h6: {
    color: '#2196f3',
    [theme.breakpoints.up('sm')]: {
      fontSize: 34,
    },
  },
  subtitle1: {
    [theme.breakpoints.up('sm')]: {
      fontSize: 24,
    },
  },
  caption: {
    [theme.breakpoints.up('sm')]: {
      fontSize: 16,
    },
  },
  h5: {
    color: '#607D8B',
    marginBottom: 20,
    marginTop: 20,
    [theme.breakpoints.up('sm')]: {
      fontSize: 48,
    },
  },
  paragraph: {
    [theme.breakpoints.up('sm')]: {
      fontSize: 20,
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: 24,
    },
  },
  raised: {
    backgroundColor: '#424246',
    marginTop: 10,
    [theme.breakpoints.up('md')]: {
      fontSize: 16,
    },
  },
  projectButton: {
    [theme.breakpoints.up('md')]: {
      fontSize: 20,
    },
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  contained: {
    backgroundColor: '#424246',
    color: '#fff',
    marginTop: 20,
  },
  list: {
    flexWrap: 'wrap',
    display: 'flex',
    alignContent: 'space-around',
    justifyContent: 'center',
  },
  credit: {
    position: 'relative',
    textAlign: 'center',
    paddingTop: 5,

  },
})

const Spinner = () => (
  <CircularProgress />
)

function ProjectList(props) {
  const { classes } = props
  return (
    <div className={classes.list}>
      {projects.map(project => (
        <LazyLoad
          once
          key={project.id}
          height={100}
          placeholder={<Spinner disableShrink determinate />}
        >
          <Card raised className={classes.card} key={project.id}>
            <CardHeader
              classes={{
                title: classes.h6,
                subheader: classes.subtitle1,
              }}
              title={project.projectName}
              subheader={project.h5}
            />
            <div className={classes.credit}>
              <i>
                Photo by
                <a href={project.picSource}>{project.picAuthor}</a>
                {project.picSource && (
                  <a href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">
                    on Unsplash
                  </a>
                )}
              </i>
            </div>
            <CardActionArea component={Link} to={project.pathname}>
              <CardMedia
                component="img"
                alt={project.h5}
                className={classes.media}
                src={project.img}
                srcSet={`${project.imgMobile} 320w, ${project.img} 768w`}
                sizes="100vw"
              />
              <CardContent>
                <Typography paragraph className={classes.paragraph}>
                  {project.paragraph}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions className={props.classes.actions}>
              <Button
                className={props.classes.projectButton}
                component={Link}
                to={project.pathname}
                size="medium"
                color="secondary"
              >
                Project Details
              </Button>
            </CardActions>
          </Card>
        </LazyLoad>
      ))}
    </div>
  )
}


ProjectList.propTypes = {
    classes: PropTypes.any.isRequired, // eslint-disable-line
}

function Projects(props) {
  const { classes } = props
  return (
    <Grid container className={classes.projectPage} direction="row">
      <Grid item xs />
      <Grid item xs={12} sm={10}>
        <Grid container alignItems="center" direction="column">
          {/* Title */}
          <Grid item>
            <Typography className={classes.h5} variant="h5">
              LATEST PROJECTS
            </Typography>
          </Grid>
          <Grid container direction="row" justify="center">
            <Grid
              item
              component={() => <ProjectList classes={classes} />}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs />
    </Grid>
  )
}

Projects.propTypes = {
  classes: PropTypes.any.isRequired, // eslint-disable-line
}

export default withStyles(styles)(Projects)
