import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// eslint-disable-next-line
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Typist from 'react-typist'
import CallMeNowButton from './CTA/CallMeNowButton'


const styles = theme => ({ // eslint-disable-line
  container: {
    paddingBottom: 20,
    justifyContent: 'center',
    backgroundColor: '#eee',
  },
  imageContainer: {
    backgroundImage: `url(${'https://ik.imagekit.io/ptebz7oxk/backgroundImage_BkAr6Tc2N.jpg'})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
    height: '100vh',
    backgroundColor: 'transparent',
    paddingBottom: '10px',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
  },
  titleContainer: {
    position: 'absolute',
    top: '15%',
    right: '10%',
    textAlign: 'end',
    [theme.breakpoints.up('lg')]: {
      top: '30%',
      left: '50%',
      textAlign: 'center',
      transform: 'translate(-50%,-50%)',
    },
  },
  headline: {
    color: '#607D8B',
    fontWeight: 'bold',
    [theme.breakpoints.up('sm')]: {
      fontSize: 34,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 48,
      right: 160,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 84,
    },
  },
  subheading: {
    color: '#607D8B',
    fontWeight: 'bold',
    [theme.breakpoints.up('sm')]: {
      fontSize: 24,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 34,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 64,
    },
  },
  title: {
    textAlign: 'center',
    fontWeight: '400',
    color: '#607D8B',
    marginTop: 20,
    marginBottom: 10,
    [theme.breakpoints.down('sm')]: {
      fontSize: 24,
      padding: 20,
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: 24,
      padding: 30,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 34,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 48,
    },
  },
  ctaContainer: {
    paddingBottom: 30,
  },
  button: {
    color: '#607D8B',
    [theme.breakpoints.up('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 20,
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: 24,
    },
  },
})

const Home = (props) => {
  const { classes } = props
  return (
    <Grid className={classes.container} container direction="column">
      <Grid item>
        {/* Container for title & Subtitle */}
        <Grid
          container
          className={classes.imageContainer}
          justify="center"
          direction="column"
        >
          {/* Title & Subtitle */}
          <Grid item className={classes.titleContainer}>
            <Typography variant="h5" className={classes.headline}>
              FABIAN STARKE
            </Typography>
            <Typography variant="subtitle1" className={classes.subheading}>
              WEB DEVELOPPER
            </Typography>
          </Grid>
        </Grid>
        {/* Intro phrase */}
        <Grid item>
          <Typist className={classes.title}>
            <span>I am a passionate Web developper focusing on</span>
            <strong> design</strong>
            .
            <Typist.Backspace count={7} delay={200} />
            <span>
              <strong> clean code</strong>
              .
            </span>
            <Typist.Backspace count={11} delay={200} />
            <span>
              <strong> your buisness success</strong>
              .
            </span>
          </Typist>
        </Grid>
        {/* CTA Buttons */}
        <Grid container className={classes.ctaContainer} direction="row">
          <Grid item xs />
          <Grid item>
            <CallMeNowButton />
          </Grid>
          <Grid item xs />
        </Grid>
        {/* Button link to About me section */}
        <Grid container className={classes.button} direction="row">
          <Grid item xs />
          <Grid item>
            <Button
              className={classes.button}
              size="medium"
              component={Link}
              to="/AboutMe"
            >
              More about me
            </Button>
          </Grid>
          <Grid item xs />
        </Grid>
      </Grid>
    </Grid>
  )
}

Home.propTypes = {
  classes: PropTypes.any.isRequired, // eslint-disable-line
}

export default withStyles(styles)(Home)
