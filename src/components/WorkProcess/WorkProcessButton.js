import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// eslint-disable-next-line
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

const styles = theme => ({ // eslint-disable-line
  root: {
    backgroundColor: '#eee',
    textAlign: 'center',
    padding: 24,
  },
  button: {
    height: 80,
  },
  closeIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  body1: {
    marginTop: 20,
    textAlign: 'justify',
    [theme.breakpoints.up('sm')]: {
      fontSize: 20,
      padding: 40,
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 24,
      padding: 50,
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 32,
      paddingLeft: 150,
      paddingRight: 150,
    },
    [theme.breakpoints.up('xl')]: {
      fontSize: 48,
      padding: 250,
    },
  },
})
class WorkProcessButton extends Component {
  static propTypes = {
    classes: PropTypes.any.isRequired, // eslint-disable-line
    text: PropTypes.string.isRequired,
    svg: PropTypes.any.isRequired, //eslint-disable-line
  }

  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true })
  };

  handleClose = () => {
    this.setState({ open: false })
  };

  render() {
    const { classes, text, svg } = this.props
    const { open } = this.state
    return (
      <div>
        <Button onClick={this.handleClickOpen} className={classes.button}><div /></Button>
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogContent>
            <DialogActions>
              <IconButton className={classes.closeIcon} onClick={this.handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
            </DialogActions>
          </DialogContent>
          <DialogContent className={classes.root}>
            <div>{svg}</div>
            <DialogContentText className={classes.body1} variant="body1">
              {text}
            </DialogContentText>
          </DialogContent>
          <DialogContent />
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles)(WorkProcessButton)
