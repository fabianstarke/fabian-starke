import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// eslint-disable-next-line
import Grid from '@material-ui/core/Grid'
import Hidden from '@material-ui/core/Hidden'
import Typography from '@material-ui/core/Typography'
import ReactGA from 'react-ga'
import WorkProcessButton from './WorkProcessButton'
import CallMeNowButton from '../CTA/CallMeNowButton'
import * as Work from './WorkProcessIcons'
import WorkProcessCycle from './WorkProcessCycle.svg'

ReactGA.pageview("/WorkProcess")

const styles = theme => ({ // eslint-disable-line
  container: {
    marginTop: 100,
    alignItems: 'center',
  },
  headline: {
    marginBottom: 20,
    color: '#607D8B',
    [theme.breakpoints.up('sm')]: {
      fontSize: 48,
    },
  },
  body1: {
    marginTop: 20,
    [theme.breakpoints.up('xs')]: {
      marginLeft: 20,
      marginRight: 20,
      fontSize: 14,
    },
    [theme.breakpoints.up('md')]: {
      marginLeft: 50,
      marginRight: 50,
      fontSize: 24,
    },
    [theme.breakpoints.up('2000')]: {
      fontSize: 28,
    },
  },
  button: {
    marginTop: 30,
    marginBottom: 30,
  },
  iconContainer: {
    position: 'relative',
    backgroundImage: `url(${WorkProcessCycle})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    height: 300,
    width: 300,
    [theme.breakpoints.up('sm')]: {
      width: 400,
      height: 400,
    },
  },
  visionButton: {
    position: 'absolute',
    right: '35%',
    [theme.breakpoints.up('sm')]: {
      right: '40%',
    },
  },
  feasibilityButton: {
    position: 'absolute',
    right: 15,
    top: 70,
    [theme.breakpoints.up('sm')]: {
      right: 30,
      top: 90,
    },
  },
  prototypingButton: {
    position: 'absolute',
    right: 60,
    bottom: 20,
    [theme.breakpoints.up('sm')]: {
      right: 80,
      bottom: 50,
    },
  },
  learnButton: {
    position: 'absolute',
    left: 40,
    bottom: 10,
    [theme.breakpoints.up('sm')]: {
      left: 75,
      bottom: 45,
    },
  },
  developementButton: {
    position: 'absolute',
    left: 5,
    top: 70,
    [theme.breakpoints.up('sm')]: {
      left: 40,
      top: 80,
    },
  },
  phaseWrapper: {
    width: 350,
    textAlign: 'center',
    padding: 30,
    marginBottom: 40,
  },
})

const phases = {
  Visioning:
    'This phase focuses on the creativity. Definition of  the product strategy, identify the main KPI’s, do problem boxing, ropose solutions and design.',
  Feasibility: 'During this phase, all the technical, budgetary and legal aspects should be reviewed in details in order to validate or not the projects feasibility.',
  Prototyping:
  'A prototype is developped in order to proceed to tests. Time could vary depending to the fact that the wireframe is directly used or if all components are coded.',
  Learn: 'A crucial part of the process. Qualitative and quantitavie tests will be proceeded during this phase. Feedbacks will be analyzed and taken into consideration carefully.',
  Developement:
  'Now its time to developp your web and mobile app based on all the previous phases. Depending on the complexity of the app the time range can vary a lot.',

}
function WorkProcess(props) {
  const { classes } = props
  return (
    <Grid container direction="row">
      <Grid item xs />
      <Grid item lg={8}>
        <Grid container className={classes.container} direction="column">
          <Grid item>
            <Typography
              className={classes.headline}
              align="center"
              variant="h5"
            >
              WORK PROCESS
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={classes.body1}
              paragraph
              align="center"
              variant="body1"
            >
              My focus is the success of your business. Therefore I am using a
              product oriented process in order to reach your main KPI’s.
            </Typography>
            <Hidden mdUp>
              <Typography
                className={classes.body1}
                paragraph
                align="center"
                variant="body1"
              >
                Click on the icons to get an insight of each phase.
              </Typography>
            </Hidden>
          </Grid>
          <Hidden only={['xs', 'sm']}>
            <Grid item>
              <Grid
                container
                direction="row"
                style={{ justifyContent: 'center' }}
              >
                <Grid item>
                  <Grid
                    container
                    className={classes.phaseWrapper}
                    direction="column"
                  >
                    <Grid item style={{ paddingBottom: 30 }}>
                      {Work.Visioning}
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="h6"
                        style={{ textAlign: 'justify', fontWeight: 400 }}
                      >
                        {phases.Visioning}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid
                    container
                    className={classes.phaseWrapper}
                    direction="column"
                  >
                    <Grid item style={{ paddingBottom: 30 }}>
                      {Work.Feasibility}
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="h6"
                        style={{ textAlign: 'justify', fontWeight: 400 }}
                      >
                        {phases.Feasibility}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid
                    container
                    className={classes.phaseWrapper}
                    direction="column"
                  >
                    <Grid item style={{ paddingBottom: 30 }}>
                      {Work.Prototyping}
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="h6"
                        style={{ textAlign: 'justify', fontWeight: 400 }}
                      >
                        {phases.Prototyping}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid
                    container
                    className={classes.phaseWrapper}
                    direction="column"
                  >
                    <Grid item style={{ paddingBottom: 30 }}>
                      {Work.Learn}
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="h6"
                        style={{ textAlign: 'justify', fontWeight: 400 }}
                      >
                        {phases.Learn}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid
                    container
                    className={classes.phaseWrapper}
                    direction="column"
                  >
                    <Grid item style={{ paddingBottom: 30 }}>
                      {Work.Developement}
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="h6"
                        style={{ textAlign: 'justify', fontWeight: 400 }}
                      >
                        {phases.Developement}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Hidden>
          <Hidden mdUp>
            <Grid item className={classes.iconContainer}>
              <div className={classes.visionButton}>
                <WorkProcessButton
                  svg={Work.Visioning}
                  text={phases.Visioning}
                />
              </div>
              <div className={classes.feasibilityButton}>
                <WorkProcessButton
                  svg={Work.Feasibility}
                  text={phases.Feasibility}
                />
              </div>
              <div className={classes.prototypingButton}>
                <WorkProcessButton
                  svg={Work.Prototyping}
                  text={phases.Prototyping}
                />
              </div>
              <div className={classes.learnButton}>
                <WorkProcessButton svg={Work.Learn} text={phases.Learn} />
              </div>
              <div className={classes.developementButton}>
                <WorkProcessButton
                  svg={Work.Developement}
                  text={phases.Developement}
                />
              </div>
            </Grid>
          </Hidden>
          <Grid item>
            <Typography
              className={classes.body1}
              paragraph
              align="justify"
              variant="body1"
            >
              Depending on the phase of your product, of course you will have
              different needs. My main focus is the prototyping and
              developement stages.I am also used to be part of the whole
              process and can offer my services or provide recommendations for
              all the non-technical aspects.
            </Typography>
          </Grid>
          <Grid item className={classes.button}>
            <CallMeNowButton />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs />
    </Grid>
  )
}
WorkProcess.propTypes = {
  classes: PropTypes.any.isRequired, // eslint-disable-line
}

export default withStyles(styles)(WorkProcess)
