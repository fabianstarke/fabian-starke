import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// eslint-disable-next-line
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Grid from '@material-ui/core/Grid'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import MenuList from './MenuList'
import Logo from '../assets/logos/whiteLogoThin.png'
import Logo2 from '../assets/logos/blackLogo.png'
import ReactGA from 'react-ga'

const styles = theme => ({ // eslint-disable-line
  root: {
    alignItems: 'center',
    [theme.breakpoints.up('md')]: {
      paddingLeft: 150,
    },
  },
  headerMenu: {
     width: 'auto',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
    padding: 10,
  },
  logo: {
    maxHeight: 44,
  },
  menuIcon: {
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  headerMenuContainer: {
    flexDirection: 'row',
  },
  list: {
    display: 'flex',
    flexDirection: 'row',
    // flexGrow: 1,
    paddingLeft: 30,
    justifyContent: 'flex-end',
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 70,
    },
  },
})
class Header extends Component {
  static propTypes = {
    classes: PropTypes.any.isRequired, // eslint-disable-line
  }

  state = {
    auth: true,
    anchorEl: null,
    top: false,
    backgroundColor: 'transparent',
    color: '#607D8B',
  };

componentDidMount(){
  window.addEventListener('scroll', this.handleScroll)
}

toggleDrawer = (side, open) => () => {
  this.setState({
    [side]: open,
  })
};

 handleChange = (event, checked) => {
   this.setState({ auth: checked })
 };


handleScroll = () => {
  if(window.scrollY > 200){
    this.setState({ backgroundColor: '#424246', color: 'white' })
  } else this.setState({ backgroundColor: 'transparent', color: '#607D8B' })
}

 handleClose = () => {
   this.setState({ anchorEl: null })
 };

 track = (page) => {
   ReactGA.pageview(page)
 }

 render() {
   const { classes } = this.props
   const { auth, anchorEl, top, backgroundColor, color } = this.state
   const open = Boolean(anchorEl)
   return (
     <AppBar position="fixed" style={{ background: backgroundColor, boxShadow: 'none' }}>
       <Toolbar>
         <Grid container className={classes.root} direction="row">
           {/* logo */}
           <Grid component={Link} to="/" item onClick={this.track('/Home')}>
             <img className={classes.logo} src={backgroundColor === 'transparent' ? Logo : Logo2 } alt="logo" />
           </Grid>
           {/* Menu List */}
           <Grid item className={classes.list} xs>
             <List className={classes.list} component="nav">
               <ListItem component={Link} to="/" className={classes.headerMenu} onClick={this.track('/Home')}>
                 <ListItemText primary={<Typography variant="subtitle1" style={{ color: color}}>Home</Typography>} />
               </ListItem>
               <ListItem component={Link} to="Projects" className={classes.headerMenu} onClick={this.track('/MyProjects')}>
                <ListItemText primary={<Typography variant="subtitle1" style={{ color: color}}>Projects</Typography>} />
               </ListItem>
               <ListItem component={Link} to="WorkProcess" className={classes.headerMenu} onClick={this.track('/MyWorkProcess')}>
                <ListItemText primary={<Typography variant="subtitle1" style={{ color: color}}>Work Process</Typography>} />
               </ListItem>
               <ListItem component={Link} to="AboutMe" className={classes.headerMenu} onClick={this.track('/AboutMe')}>
                <ListItemText primary={<Typography variant="subtitle1" style={{ color: color}}>About Me</Typography>} />
               </ListItem>
             </List>
           </Grid>
           {/* MenuIcon */}
           <Grid item>
             {auth && (
             <div>
               <IconButton
                 className={classes.menuIcon}
                 aria-label="burger-menu"
                 aria-owns={open ? 'menu-appbar' : null}
                 aria-haspopup="true"
                 onClick={this.toggleDrawer('top', true)}
                 color={backgroundColor === '#424246' ? 'inherit' : 'default'}
               >
                 <MenuIcon />
               </IconButton>
               <Drawer
                 anchor="top"
                 open={top}
                 onClose={this.toggleDrawer('top', false)}
               >
                 <div
                   aria-label="Drawer"
                   className="Drawer"
                   tabIndex={0}
                   role="button"
                   onClick={this.toggleDrawer('top', false)}
                   onKeyDown={this.toggleDrawer('top', false)}
                 >
                   <MenuList />
                 </div>
               </Drawer>
             </div>
             )}
           </Grid>
         </Grid>
       </Toolbar>
     </AppBar>
   )
 }
}
export default withStyles(styles)(Header)
