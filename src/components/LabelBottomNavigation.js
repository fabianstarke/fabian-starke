import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'

const styles = theme  => ({ // eslint-disable-line
  nav: {
    display: 'flex',
    borderRadius: 4,
    backgroundColor: '#424246',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  label: {
    color: '#fff',
    fontSize: 14,
    [theme.breakpoints.up('lg')]: {
      fontSize: 24,
    },
  },
  root: {
    paddingLeft: 0,
    paddingRight: 0,
    minWidth: 50,
  },
})
class LabelBottomNavigation extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired, //eslint-disable-line
  }

  state = {
    value: 'Home',
  };

  handleChange = (event, value) => {
    this.setState({ value })
  };

  render() {
    const { classes } = this.props
    const { value } = this.state

    return (
      <BottomNavigation
        value={value}
        showLabels
        onChange={this.handleChange}
        className={classes.nav}
      >
        <BottomNavigationAction component={Link} to="/Home" classes={{ root: classes.root, label: classes.label, selected: classes.selected }} label="Home" value="Home" />
        <BottomNavigationAction component={Link} to="/Projects" classes={{ selected: classes.selected, root: classes.root, label: classes.label }} label="Projects" value="Projects" />
        <BottomNavigationAction component={Link} to="/WorkProcess" classes={{ selected: classes.selected, root: classes.root, label: classes.label }} label="Process" value="Process" />
        <BottomNavigationAction component={Link} to="/AboutMe" classes={{ selected: classes.selected, root: classes.root, label: classes.label }} label="About" value="About" />
      </BottomNavigation>
    )
  }
}

export default withStyles(styles)(LabelBottomNavigation)
