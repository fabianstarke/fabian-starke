import React from 'react'
import { render } from 'react-dom'
import PropTypes from 'prop-types'
import { BrowserRouter } from 'react-router-dom'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import blue from '@material-ui/core/colors/blue'
import red from '@material-ui/core/colors/red'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'


const theme = createMuiTheme({ // eslint-disable-line
  palette: {
    primary: blue,
    secondary: red,
  },
  typography: {
    useNextVariants: true,
  },
})

class ErrorBoundary extends React.Component {
     static propTypes = {
       // eslint-disable-next-line react/forbid-prop-types
       children: PropTypes.any.isRequired,
     }

     constructor(props) {
       super(props)
       this.state = { hasError: false }
     }

     // eslint-disable-next-line no-unused-vars
     componentDidCatch(error, info) {
       // Display fallback UI
       this.setState({ hasError: true })
       // You can also log the error to an error reporting service
       // logErrorToMyService(error, info)
     }

     render() {
       if (this.state.hasError) {
         // You can render any custom fallback UI
         return (
           <h1>Something went wrong.</h1>
         )
       }
       return this.props.children
     }
}
render(
  <BrowserRouter>
    <MuiThemeProvider theme={theme}>
      <ErrorBoundary>
        <App />
      </ErrorBoundary>
    </MuiThemeProvider>
  </BrowserRouter>,
  document.getElementById('root'),
)


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
