import firebase from 'firebase/app'
import 'firebase/database'

// Initialize database

const config = {
  apiKey: 'AIzaSyBgUS_YgNNKwsCfy6Fivl-NyYZK5dIgDXg',
  authDomain: 'contactform-954ef.firebaseapp.com',
  databaseURL: 'https://contactform-954ef.firebaseio.com',
}

firebase.initializeApp(config)

export default firebase
