import React from 'react'
// import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, BrowserRouter, Route, withRouter, Link } from 'react-router-dom'
// eslint-disable-next-line
import Grid from '@material-ui/core/Grid'
import ReactGA from 'react-ga'
import Header from './components/Header'
import Home from './components/Home'
import AboutMe from './components/AboutMe'
import Projects from './components/Projects/Projects'
import WorkProcess from './components/WorkProcess/WorkProcess'
import Footer from './components/Footer'
import ProjectDetailAxtrade from './components/Projects/ProjectDetailAxtrade'
import ProjectDetailCda from './components/Projects/ProjectDetailCda'
import ProjectDetailFlashBruxellois from './components/Projects/ProjectDetailFlashBruxellois'
import ProjectDetailFabianstarke from './components/Projects/ProjectDetailFabianstarke'
import ProjectDetailStaces from './components/Projects/ProjectDetailStaces'
import ProjectDetailZilo from './components/Projects/ProjectDetailZilo'
import ProjectDetailReflexionExpat from './components/Projects/ProjectDetailReflexionExpat'
import ProjectDetailIbiza from './components/Projects/ProjectDetailIbiza'
import ProjectDetailAvoy from './components/Projects/ProjectDetailAvoy'
import ProjectDetailChingu from './components/Projects/ProjectDetailChingu'
import ProjectDetailWedding from './components/Projects/ProjectDetailWedding'
import ProjectDetailGoeland from './components/Projects/ProjectDetailGoeland'
import ProjectDetailDevDashboard from './components/Projects/ProjectDetailDevDashboard'

ReactGA.initialize('UA-141679990-1')
ReactGA.pageview('/Home')

function App() {
  return (
    <BrowserRouter>
      <div>
        <Header />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <Grid container direction="column">
                <Grid item>
                  <Home />
                  <WorkProcess id="work-process" />
                  <Projects />
                </Grid>
              </Grid>
            )}
          />
          <Route path="/Home" component={Home} />
          <Route path="/AboutMe" component={AboutMe} />
          <Route path="/WorkProcess" component={WorkProcess} />
          <Route path="/Projects" component={Projects} />
          <Route path="/Axtrade" component={ProjectDetailAxtrade} />
          <Route path="/CarreDartistes" component={ProjectDetailCda} />
          <Route
            path="/FlashBruxellois"
            component={ProjectDetailFlashBruxellois}
          />
          <Route path="/MyPortfolio" component={ProjectDetailFabianstarke} />
          <Route path="/Staces" component={ProjectDetailStaces} />
          <Route path="/Zilo" component={ProjectDetailZilo} />
          <Route path="/Ibiza" component={ProjectDetailIbiza} />
          <Route
            path="/ReflexionsDexpat"
            component={ProjectDetailReflexionExpat}
          />
          <Route path="/Avoy" component={ProjectDetailAvoy} />
          <Route path="/Chingu" component={ProjectDetailChingu} />
          <Route path="/WeddingPage" component={ProjectDetailWedding} />
          <Route path="/Goeland" component={ProjectDetailGoeland} />
          <Route path="/DevDashboard" component={ProjectDetailDevDashboard} />
        </Switch>
        <Footer />
      </div>
    </BrowserRouter>
  )
}

export default App
